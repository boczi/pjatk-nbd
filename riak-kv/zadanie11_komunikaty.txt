
---------------------
1. wrzuci do bazy dokument,
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Thu, 14 Jun 2018 21:44:52 GMT
Content-Type: application/json
Content-Length: 0


---------------------
2. pobierze go i wypisze,
HTTP/1.1 200 OK
X-Riak-Vclock: a85hYGBgzGDKBVI8+xkWvv8fYuIHZLNlMCUy5rEyXImdfoUvCwA=
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Link: </buckets/zad11>; rel="up"
Last-Modified: Thu, 14 Jun 2018 21:44:52 GMT
ETag: "221FEORHDIPHyUO3NwuO7g"
Date: Thu, 14 Jun 2018 21:44:52 GMT
Content-Type: application/json
Content-Length: 64

{"first_name":"Andrzej","last_name":"Boczek","status":"student"}
---------------------
3. zmodyfikuje go
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Thu, 14 Jun 2018 21:44:52 GMT
Content-Type: application/json
Content-Length: 0


---------------------
4. następnie pobierze i wypisze,
HTTP/1.1 200 OK
X-Riak-Vclock: a85hYGBgzGDKBVI8+xkWvv8fYuIHZLNlMCUy5bEyXImdfoUvCwA=
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Link: </buckets/zad11>; rel="up"
Last-Modified: Thu, 14 Jun 2018 21:44:52 GMT
ETag: "2l8m3hO4PTb4mFnOnOArHQ"
Date: Thu, 14 Jun 2018 21:44:53 GMT
Content-Type: application/json
Content-Length: 63

{"first_name":"Andrzej","last_name":"Boczek","status":"worker"}
---------------------
5. a na końcu usunie go
HTTP/1.1 204 No Content
Vary: Accept-Encoding
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Thu, 14 Jun 2018 21:44:53 GMT
Content-Type: application/json
Content-Length: 0


---------------------
6. i spróbuje pobrać z bazy
HTTP/1.1 404 Object Not Found
X-Riak-Vclock: a85hYGBgzGDKBVI8+xkWvv8fYuIHZLNlMCUy57EyXI2dfoUvCwA=
X-Riak-Deleted: true
Server: MochiWeb/1.1 WebMachine/1.10.9 (cafe not found)
Date: Thu, 14 Jun 2018 21:44:53 GMT
Content-Type: text/plain
Content-Length: 10

not found
