#! /bin/bash
#  Napisz program, który
#  1. wrzuci do bazy dokument,
#  2. pobierze go i wypisze,
#  3. zmodyfikuje go,
#  4. następnie pobierze i wypisze,
#  5. a na końcu usunie go
#  6. i spróbuje pobrać z bazy

DBURL=http://192.168.99.100:8098
BUCKET=zad11

function get() {
  curl -i $DBURL/buckets/$BUCKET/keys/$1
}
function put() {
  curl -XPUT -d"$2" -H "Content-Type: application/json" -i $DBURL/buckets/$BUCKET/keys/$1
}
function delete() {
  curl -XDELETE -i $DBURL/buckets/$BUCKET/keys/$1
}




echo -e "\n---------------------"
echo  1. wrzuci do bazy dokument,
put 'euifheiuh' '{"first_name":"Andrzej","last_name":"Boczek","status":"student"}'


echo -e "\n---------------------"
echo  2. pobierze go i wypisze,
get 'euifheiuh'


echo -e "\n---------------------"
echo  3. zmodyfikuje go
put 'euifheiuh' '{"first_name":"Andrzej","last_name":"Boczek","status":"worker"}'

echo -e "\n---------------------"
echo  4. następnie pobierze i wypisze,
get 'euifheiuh'


echo -e "\n---------------------"
echo  5. a na końcu usunie go
delete 'euifheiuh'

echo -e "\n---------------------"
echo  6. i spróbuje pobrać z bazy
get 'euifheiuh'
