5. Uszereguj trasy którymi można dostać się z Darjeeling na Sandakphu według dystansu

match  (a:town{name:'Darjeeling'}), (b:peak{name:'Sandakphu'})
with a,b
match path= (a) -[*{winter:'true'}]-> (b)
RETURN path, length(path) as dlugosc
order by dlugosc
