-- Znajdź trasy którymi można dostać się z Darjeeling na Sandakphu, mające najmniejszą ilość etapów

match  (a:town{name:'Darjeeling'}), (b:peak{name:'Sandakphu'})
with a,b
match shortest=shortestPath((a)-[*]->(b))
with a,b,length(shortest) as len
match path= (a) -[*]-> (b)
where length(path)=len

RETURN path
