4. Znajdź mające najmniej etapów trasy którymi można dostać się z Darjeeling na Sandakphu i które mogą być wykorzystywane zimą



match  (a:town{name:'Darjeeling'}), (b:peak{name:'Sandakphu'})
with a,b
match shortest=shortestPath((a)-[*]->(b))
with a,b,length(shortest) as len
match path= (a) -[*{winter:'true'}]-> (b)
where length(path)=len

RETURN path
