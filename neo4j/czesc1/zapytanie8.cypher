8. Zestaw zapytań powodujących uzupełnienie bazy danych o film
  Captain America: The First Avenger
  wraz z uzupełnieniem informacji
  o reżyserze, scenarzystach i odtwórcach głównych ról
  (w oparciu o skrócone informacje z IMDB - http://www.imdb.com/title/tt0458339/)
  + zapytanie pokazujące dodany do bazy film wraz odtwórcami głównych
  ról, scenarzystą i reżyserem.
  Plik SVG ma pokazywać wynik ostatniego zapytania.

CREATE (CaptainAmerica:Movie {title:'Captain America: The First Avenger', released:2011, tagline:'When patriots become heroes'})
CREATE (ChrisEvans:Person {name:'Chris Evans', born:1981})
CREATE (HayleyAtwell:Person {name:'Hayley Atwell', born:1982})
CREATE (SebastianStan:Person {name:'Sebastian Stan', born:1982})
CREATE (TommyLeeJones:Person {name:'Tommy Lee Jones', born:1946})
CREATE (DominicCooper:Person {name:'Dominic Cooper', born:1978})
CREATE (RichardArmitage:Person {name:'Richard Armitage', born:1971})
CREATE (StanleyTucci:Person {name:'Stanley Tucci', born:1960})
CREATE (SamuelLJackson:Person {name:'Samuel L. Jackson', born:1948})
CREATE (TobyJones:Person {name:'Toby Jones', born:1966})
CREATE (NealMcDonough:Person {name:'Neal McDonough', born:1966})
CREATE (DerekLuke:Person {name:'Derek Luke', born:1974})
CREATE (KennethChoi:Person {name:'Kenneth Choi', born:1971})
CREATE (JJFeild:Person {name:'JJ Feild', born:1978})
CREATE (BrunoRicci:Person {name:'Bruno Ricci'})

CREATE (JoeJohnston:Person {name:'Joe Johnston', born:1950})
CREATE (ChristopherMarkus:Person {name:'Christopher Markus'})
CREATE (StephenMcFeely:Person {name:'Stephen McFeely'})


CREATE
  (ChrisEvans)-[:ACTED_IN {roles:['Captain America','Steve Rogers']}]->(CaptainAmerica),
  (HayleyAtwell)-[:ACTED_IN {roles:['Peggy Carter']}]->(CaptainAmerica),
  (SebastianStan)-[:ACTED_IN {roles:['James Buchanan Barnes']}]->(CaptainAmerica),
  (TommyLeeJones)-[:ACTED_IN {roles:['Colonel Chester Phillips']}]->(CaptainAmerica),
  (Hugo)-[:ACTED_IN {roles:['Johann Schmidt','Red Skull']}]->(CaptainAmerica),
  (DominicCooper)-[:ACTED_IN {roles:['Howard Stark']}]->(CaptainAmerica),
  (RichardArmitage)-[:ACTED_IN {roles:['Heinz Kruger']}]->(CaptainAmerica),
  (StanleyTucci)-[:ACTED_IN {roles:['Dr. Abraham Erskine']}]->(CaptainAmerica),
  (SamuelLJackson)-[:ACTED_IN {roles:['Nick Fury']}]->(CaptainAmerica),
  (TobyJones)-[:ACTED_IN {roles:['Dr. Arnim Zola']}]->(CaptainAmerica),
  (NealMcDonough)-[:ACTED_IN {roles:['Timothy Dugan']}]->(CaptainAmerica),
  (DerekLuke)-[:ACTED_IN {roles:['Gabe Jones']}]->(CaptainAmerica),
  (KennethChoi)-[:ACTED_IN {roles:['Jim Morita']}]->(CaptainAmerica),
  (JJFeild)-[:ACTED_IN {roles:['James Montgomery Falsworth']}]->(CaptainAmerica),
  (BrunoRicci)-[:ACTED_IN {roles:['	Jacques Dernier']}]->(CaptainAmerica),


  (JoeJohnston)-[:DIRECTED]->(CaptainAmerica),

  (ChristopherMarkus)-[:WROTE]->(CaptainAmerica),
  (StephenMcFeely)-[:WROTE]->(CaptainAmerica)



-- Zapytanie:

match (ca:Movie{title:'Captain America: The First Avenger'}) <-[r]- (p:Person)
return  type(r) as udzial , p.name, r.roles
order by udzial
