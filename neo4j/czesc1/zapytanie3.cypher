-- 3. Reżyserzy filmów, w których grał Hugo Weaving

match (hugo:Person {name:"Hugo Weaving"}) -[:ACTED_IN]->(movie:Movie) <-[:DIRECTED]- (director:Person)
return director.name
