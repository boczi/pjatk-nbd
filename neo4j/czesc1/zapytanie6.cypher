-- 6. Listę osób, które napisały scenariusz filmu,
-- które wyreżyserowały wraz z tytułami takich filmów
-- (koniunkcja – ten sam autor scenariusza i reżyser)

match (dir:Person) -[:DIRECTED]-> (movie:Movie) <-[:WROTE]-(author:Person)
where dir=author
return dir.name, movie.title


-- Ewentualnie:
match (dir:Person) -[:DIRECTED]-> (movie:Movie) <-[:WROTE]-(dir:Person)
return dir, movie
