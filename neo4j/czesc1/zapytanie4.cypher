-- 4. Wszystkie osoby, z którymi Hugo Weaving grał w tych samych filmach

match (hugo:Person {name:"Hugo Weaving"}) -[:ACTED_IN]->(movie:Movie) <-[:ACTED_IN]- (cooactor:Person)
return cooactor.name
