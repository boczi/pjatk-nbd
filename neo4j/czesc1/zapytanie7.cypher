-- 7. Listę filmów, w których grał zarówno Hugo Weaving jak i Keanu Reeve

match (hugo:Person {name:'Hugo Weaving'}) -[:ACTED_IN]-> (movie:Movie) <-[:ACTED_IN]-(keanu:Person{name:'Keanu Reeves'})
return movie.title
