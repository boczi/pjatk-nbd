-- 2. Wszystkie filmy, w których grał Hugo Weaving

match (h:Person {name:"Hugo Weaving"}) -[:ACTED_IN]->(movie:Movie)
return movie.title
