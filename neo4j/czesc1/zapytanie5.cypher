-- 5. Listę aktorów (aktor = osoba, która grała przynajmniej w jednym filmie)
wraz z ilością filmów, w których grali

match (actor:Person) -[:ACTED_IN]->(m:Movie)
return actor.name, count(m) as liczba_filmow
