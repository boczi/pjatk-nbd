7. Uszereguj linie lotnicze według ilości miast, pomiędzy którymi oferują połączenia
(unikalnych miast biorących udział w relacjach :ORIGIN i :DESTINATION węzłów typu
Flight obsługiwanych przez daną linię)


match (a:Flight)-[e]->(d:Airport)
return distinct a.airline, count(distinct d.name)
