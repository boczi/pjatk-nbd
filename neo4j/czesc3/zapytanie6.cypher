6. Znajdź najtańsze połączenie z Los Angeles (LAX) do Dayton (DAY) w klasie biznes


match (f:Flight)
with f
match (t:Ticket{class:'business'}) --> (f)
with f, min(t.price) as min_price
match (orig:Airport) <-[:ORIGIN]- (f) -[:DESTINATION]-> (dest:Airport)
with f,min_price,orig,dest
create (orig) -[:CONNECTION_BUSINESS{min_price:min_price,distance:f.distance}]->(dest)


match path=((la:Airport{name:'LAX'}) -[r:CONNECTION_BUSINESS*1..4]- (p:Airport{name:'DAY'}))
WITH REDUCE(price=0, rel IN r | price + rel.min_price) AS min_price, path
return path, min_price
order by min_price
limit 1
