8. Znajdź najtańszą trasę łączącą 3 różne porty lotnicze

match (a1:Airport) -[c1:CONNECTION]->
	(a2:Airport) -[c2:CONNECTION]->
    (a3:Airport)
where a1<>a2 and a2<>a3 and a1<>a3
return a1, c1,a2,c2,a3
order by (c1.min_price+c2.min_price)
limit 1
