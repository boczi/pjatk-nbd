5. Znajdź najtańsze połączenie z Los Angeles (LAX) do Dayton (DAY)


match path=((la:Airport{name:'LAX'}) -[r:CONNECTION*1..4]- (p:Airport{name:'DAY'}))
WITH REDUCE(price=0, rel IN r | price + rel.min_price) AS min_price, path
return path, min_price
order by min_price
limit 1
