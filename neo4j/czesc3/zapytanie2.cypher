2. Uszereguj porty lotnicze według ilości rozpoczynających się w nich lotów

match (a:Airport) <-[:ORIGIN]- (f:Flight)
return a.name, count(f) as liczba
order by liczba desc
