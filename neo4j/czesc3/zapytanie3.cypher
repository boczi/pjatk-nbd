3. Znajdź wszystkie porty lotnicze, do których da się dolecieć
(bezpośrednio lub z przesiadkami) z Los Angeles (LAX) wydając mniej niż 3000


// Tworzymy połączenia
match (f:Flight)
with f
match (t:Ticket) --> (f)
with f, min(t.price) as min_price
match (orig:Airport) <-[:ORIGIN]- (f) -[:DESTINATION]-> (dest:Airport)
with f,min_price,orig,dest
create (orig) -[:CONNECTION{min_price:min_price,distance:f.distance}]->(dest)


match p=((la:Airport{name:'LAX'})-[c:CONNECTION*1..6]->(d:Airport))
with reduce(sum = 0, r IN relationships(p)| sum + r.min_price) AS cost, p as p, d as d
where cost<=3000
return distinct d
