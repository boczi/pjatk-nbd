println("8. Napisz funkcję usuwającą zera z listy wartości całkowitych (tzn.zwracającą nową listę,zawierającą wartościz oryginalnej listy z wyjątkiem “0”)");

def removeZeros(l:List[Integer]) = {
  l.filter(_ != 0);
}


removeZeros(List[Integer](1,2,34,0,5,7,0,87,4,0,0,0,4,2,1)).foreach(println);
