println("4. Zaprezentuj działanie Option na dowolnym przykładzie (np. mapy, w której wyszukujemy wartości po kluczu)");

val remedy = Map(
  "headake" -> "Paracetamol",
  "insomia" -> "Benosen",
  "stomachake" -> "Calcium"
);

for ( d <- List("headake","insomia", "cancer") ) {
  var cure=remedy.get(d);
  cure match {
    case Some(medicine) => println ("Cure for "+d+" is "+medicine);
    case None => println ("There is no cure for "+d);
  }
}
