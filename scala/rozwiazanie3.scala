println("3. Zdefiniuj funkcję przyjmującą krotkę z 3 wartościami różnych typów i wypisującą je");


def printTuple(t:Tuple3[Any, Any, Any]):Any = {
  println(t.toString);
}
def printTuple2(t:Tuple3[Any, Any, Any]):Any = {
  println(t._1);
  println(t._2);
  println(t._3);
}


printTuple( Tuple3("Andrzej",7,true));
println(".");
printTuple2( Tuple3(8,"Mydło","Lidl"));
println(".");
printTuple2( Tuple3(22.3,55,(a:Double)=> a*a));
