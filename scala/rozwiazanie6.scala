println("6. Zdefiniuj klasę KontoBankowe z metodami wplata i wyplata oraz własnością stanKonta - własność ma być tylko do odczytu. Klasa powinna udostępniać konstruktor przyjmujący początkowy stan konta oraz drugi, ustawiający początkowy stan konta na 0.");
class KontoBankowe () {
  var _stanKonta:Double = 0.0;
  def this(saldo:Double) {
    this();
    _stanKonta=saldo;
  }
  def stanKonta=_stanKonta;
  def wplata (kwota:Double ){
    _stanKonta=_stanKonta+kwota;
  }
  def wyplata (kwota:Double ){
    _stanKonta=_stanKonta-kwota;
  }
}

var mojeKonto= new KontoBankowe(0.0);
println("Saldo: "+mojeKonto.stanKonta);
println ("->mojeKonto.wplata(1024.0);");
mojeKonto.wplata(1024.0);
println("Saldo: "+mojeKonto.stanKonta);
println ("->mojeKonto.wyplata(433.0);");
mojeKonto.wyplata(433.0);
println("Saldo: "+mojeKonto.stanKonta);
println ("->mojeKonto.wyplata(222.0);");
mojeKonto.wyplata(222.0);
println("Saldo: "+mojeKonto.stanKonta);
println ("->mojeKonto.wyplata(11.2);");
mojeKonto.wyplata(11.2);
println("Saldo: "+mojeKonto.stanKonta);
