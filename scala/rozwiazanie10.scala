println("10. Stwórz funkcję przyjmującą listę liczb rzeczywistych i zwracającą stworzoną na jej podstawie listę zawierającą wartości bezwzględne elementów z oryginalnej listy należących do przedziału <-5,12>");



def realFun(l:List[Double]) = {
  l.filter( x => (x >= -5.0 && x <= 12 )).map( x => Math.abs(x));
}


realFun(List[Double](
    3.14,
    2.71,
    0,
    -10,
    22,
    -4.22,
    0,
    102987,
    221,
    0.12
    )).foreach(println);
