println("   -> 1. Stwórz 7 elementową listę zawierającą nazwy dni tygodnia. Wypisz ją używając:");
val dow = List("Poniedziałek","Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela");

println("   -> a. Pętli for");
for (d <- dow) {println(d)}

println("   -> b. Pętli for wypisując tylko dniz nazwami zaczynającymi się na „P”");
for (d <- dow if d.startsWith("P")) { println(d)}

println("   -> c. Metody foreach");
dow.foreach(println);

println("   -> d. Pętli while");
var cur=dow;
while (!cur.isEmpty) {println(cur.head);cur=cur.tail;}


println("   -> e. Funkcji rekurencyjnej");
def reqPrint(l:List[String]):Any = {if (!l.isEmpty){println(l.head);reqPrint(l.tail)}};
reqPrint(dow);

println("   -> f. Funkcji rekurencyjnej wypisując elementy listy od końca");
def reqPrintRev(l:List[String]):Any = {if (!l.isEmpty){reqPrintRev(l.tail);println(l.head);}};
reqPrintRev(dow);

println("   -> g. Metod foldl i foldr");
println ("*foldr: "+"\n"+dow.foldRight("")(_+"\n"+_) );
println ("*foldl: "+"\n"+dow.foldLeft("")(_+"\n"+_) );


println("   -> h. Metody foldl wypisując tylko dni z nazwami zaczynającymi się na „P”");
println (dow.foldLeft("")( (l,r) => {if(r.startsWith("P")){l+"\n"+r} else {l}}));
