println("2. Stwórz mapę z nazwami produktów i cenami. Na jej podstawie wygeneruj drugą,z 10% obniżką cen. Wykorzystaj mechanizm mapowania kolekcji.");

val produkty = Map (
  "Mydło" -> 10.0,
  "Powidło" -> 22.1,
  "Sznurek" -> 9.30,
  "Ogórek" -> 12.1
);
println("Produkty: "+produkty);


val promocja =produkty.mapValues( (x) => (x*0.9));

println("Promocja: "+promocja);
