6. Zdefiniuj klasę KontoBankowe z metodami wplata i wyplata oraz własnością stanKonta - własność ma być tylko do odczytu. Klasa powinna udostępniać konstruktor przyjmujący początkowy stan konta oraz drugi, ustawiający początkowy stan konta na 0.
Saldo: 0.0
->mojeKonto.wplata(1024.0);
Saldo: 1024.0
->mojeKonto.wyplata(433.0);
Saldo: 591.0
->mojeKonto.wyplata(222.0);
Saldo: 369.0
->mojeKonto.wyplata(11.2);
Saldo: 357.8
