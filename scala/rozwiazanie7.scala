println("7. Zdefiniuj klasę Osoba z własnościami imie i nazwisko. Stwórz kilka instancji tej klasy. Zdefiniuj funkcję, która przyjmuje obiekt klasy osoba i przy pomocy Pattern Matching wybiera i zwraca napis zawierający przywitanie danej osoby. Zdefiniuj 2-3 różne przywitania dla konkretnych osób (z określonym imionami lub nazwiskami) oraz jedno domyślne.");

class Osoba(val imie: String, val nazwisko: String ) {}

var andrzej = new Osoba("Andrzej","Duda");
var jarek = new Osoba("Jarosław","Kaczyński");
var basia = new Osoba("Barbara","Skrzypek");
var joeDoe = new Osoba("Jan", "Kowalski");

def greet(o:Osoba) : String = {
  (o.imie,o.nazwisko) match {
    case ("Andrzej","Duda") => "Dzień dobry, Panie Adrianie"
    case ("Jarosław","Kaczyński") => "Uszanowanie panie Prezesie"
    case ("Barbara","Skrzypek") => "Dzień dobry, Pani Basieńko"
    case ( _ , _) => "Witaj, "+o.imie+" "+o.nazwisko
  }
}
println(greet(jarek));
println(greet(andrzej));
println(greet(basia));
println(greet(joeDoe));
