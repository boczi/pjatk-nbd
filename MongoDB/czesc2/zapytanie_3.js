// Przy pomocy operacji map-reduce i tam gdzie to możliwe frameworku do agregacji
// (czyli w przypadku niektórych zadań należy dostarczyć 2 rozwiązania
// by móc je zaliczyć) znajdź następujące informacje:

// 3. Listę unikalnych zawodów;

db.people.aggregate(
  {$group: {_id: "$job"}}
);

db.people.mapReduce(
  function () {emit(this.job,null);},
  function () { return null},
  {out:{inline:1}}
)
