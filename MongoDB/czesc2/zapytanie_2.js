// Przy pomocy operacji map-reduce i tam gdzie to możliwe frameworku do agregacji
// (czyli w przypadku niektórych zadań należy dostarczyć 2 rozwiązania
// by móc je zaliczyć) znajdź następujące informacje:

// 2. Łączną ilość środków pozostałych na kartach kredytowych osób w bazie,
// w podziale na waluty;

// Ze względu na przechowywanie balansów w formie tekstowej, agregacje nie zadziałają:
db.people.aggregate(
  { $unwind: "$credit" },
  {$group: {_id: "$credit.currency", first: {$first: "$credit.balance"}, sum:{$sum: "$credit.balance"}}}
);

// Ale MapReduce jest niezawodny
db.people.mapReduce(
  function() {this.credit.map((cc) => {emit(cc.currency,parseFloat(cc.balance))}) },
  function(k,v) {return Array.sum(v) },
  {out:{inline:1}}
);
