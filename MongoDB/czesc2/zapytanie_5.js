// Przy pomocy operacji map-reduce i tam gdzie to możliwe frameworku do agregacji
// (czyli w przypadku niektórych zadań należy dostarczyć 2 rozwiązania
// by móc je zaliczyć) znajdź następujące informacje:

// 5. Średnia i łączna ilość
// środków na kartach kredytowych kobiet
// narodowości polskiej w podziale na waluty.

//db.people.find({
//  sex:'Female',
//  nationality:'Poland',
//});

// Ze względu na przechowywanie balansów w formie tekstowej, agregacje nie zadziałają:
db.people.aggregate(
  {$match:{sex:'Female',nationality:'Poland',}},
  { $unwind: "$credit" },
  {$group: {_id: "$credit.currency", first: {$first: "$credit.balance"}, sum:{$sum: "$credit.balance"}}}
);

// Ale MapReduce jest niezawodny
db.people.mapReduce(
  // mapper
  function() {
    this.credit.map((cc) => {
      emit(cc.currency, {
          count:1,
          sum:parseFloat(cc.balance)
        });
    }) },

  // Reducer
  function(k,v) {return v.reduce(
    (a,b) => { return {
      count:a.count+b.count,
      sum:a.sum+b.sum
    }}
  )},
  {
    out:{inline:1},
    query:{sex:'Female',nationality:'Poland'},
    finalize: function (k,v) {
      return {
        count:v.count,
        avg:v.sum/v.count,
        sum:v.sum,
      };},

  }
);
