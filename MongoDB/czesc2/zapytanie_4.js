// Przy pomocy operacji map-reduce i tam gdzie to możliwe frameworku do agregacji
// (czyli w przypadku niektórych zadań należy dostarczyć 2 rozwiązania
// by móc je zaliczyć) znajdź następujące informacje:

// 4. Średnie, minimalne i maksymalne BMI
// (waga/wzrost^2) dla osób w bazie,
// w podziale na narodowości;


db.people.mapReduce(
  // mapper
  function () {
    var h=parseFloat(this.height)/100;
    var bmi=parseFloat(this.weight) / (h*h);
    emit(this.nationality,{count:1,sum:bmi,min:bmi, max:bmi});
  },
  // Reducer
  function (k,v) {
    return v.reduce((a,b)=>{
      return {
        sum:a.sum+b.sum,
        count:a.count+b.count,
        min: ((a.min<b.min) ? a.min:b.min),
        max: ((a.max>b.max) ? a.max:b.max),
      };
    });
  },
  {out:{inline:1},
  finalize: function (k,v) {
    return {
      avg:v.sum/v.count,
      min:v.min,
      max:v.max
    };},
  }
);


// Za pomocą aggregate nie ma możliwośi obliczenia BMI ze wzglęu na
// brak konwersji napisu na liczbę:
db.people.aggregate(
  {$project:{bmi:{
    $divide: [
      "$weight",
      {$pow:[
        {$divide:["$height",100]},
        2
      ]}]}}
    }
  );
// BMI nie jest liczone
