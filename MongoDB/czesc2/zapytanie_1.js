// Przy pomocy operacji map-reduce i tam gdzie to możliwe frameworku do agregacji
// (czyli w przypadku niektórych zadań należy dostarczyć 2 rozwiązania
// by móc je zaliczyć) znajdź następujące informacje:

// 1. Średnią wagę i wzrost osób w bazie z podziałem na płeć
// (tzn. osobno mężczyzn, osobno kobiet);



db.people.mapReduce(
  // mapper
  function (){emit(this.sex, {count:1,weight:parseFloat(this.weight),height:parseFloat(this.height),})},

  // Reducer
  function (key,values) {
      return values.reduce((a,b) => {
            return {
              count: a.count+b.count,
              weight: a.weight+b.weight,
              height: a.height+b.height,
            }});
          },
  {
    out: { inline: 1 },
//    verbose: true,
    finalize: function(k,v) {return {avgWeight: v.weight/v.count,avgHeight: v.height/v.count}},
  });


// Agregacja nie zadziała ze wzgledu na przechowywanie liczb jako teksty
db.people.aggregate({$group: {_id: "$sex", avgWeight: {$avg: "$weight"}, avgHeight: {$avg: "$height"}}});
