// 9. Zastąp nazwę miasta „Moscow” przez „Moskwa” u wszystkich osób w bazie;

db.people.find({'location.city':'Moscow'},{'location.city':1});

db.people.update(
  {'location.city':'Moscow'},
  {$set: {
    location: {city:'Moskwa'}
  }},
  {multi: true}
);

db.people.find({'location.city':'Moscow'},{'location.city':1});
db.people.find({'location.city':'Moskwa'},{'location.city':1});
