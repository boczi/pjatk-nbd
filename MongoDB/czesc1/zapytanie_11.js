// 11. Usuń u wszystkich osób o zawodzie „Editor” własność „email”.

db.people.findOne ({job:'Editor'},{job:1,email:1});

db.people.update(
  {job:'Editor'},
  {$unset: {
    email:1
  }},
  {multi:true}
);

db.people.findOne ({job:'Editor'},{job:1,email:1});
