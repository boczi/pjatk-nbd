// 7. Dodaj siebie do bazy,
// zgodnie z formatem danych użytych dla innych osób
// (dane dotyczące karty kredytowej, adresu zamieszkania i wagi mogą być fikcyjne);

db.people.insert (
  {
  	"sex" : "Male",
  	"first_name" : "Andrzej",
  	"last_name" : "Boczek",
  	"job" : "IT Manager",
  	"email" : "spam@boczi.net",
  	"location" : {
  		"city" : "Warsaw",
  		"address" : {
  			"streetname" : "Koszykowa",
  			"streetnumber" : "33"
  		}
  	},
  	"description" : "ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar",
  	"height" : "180.1",
  	"weight" : "102",
  	"birth_date" : "1975-04-04T18:22:58Z",
  	"nationality" : "Poland",
  	"credit" : [
  		{
  			"type" : "visa",
  			"number" : "3029820398509809328",
  			"currency" : "PLN",
  			"balance" : "463.86"
  		}
  	]
  }
);

db.people.findOne({last_name: 'Boczek'});
