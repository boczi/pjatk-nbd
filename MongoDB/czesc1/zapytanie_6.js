// 6. Lista imion i nazwisk wszystkich osób znajdujących się w bazie
// oraz miast, w których mieszkają,
// ale tylko dla osób urodzonych w XXI wieku;

DBQuery.shellBatchSize = 100000;

// W przykładowej bazie danych daty są przechowywane jako tekst!!!
// Użyję sortowania leksykograficznego, któe przy tyk formacie daty: 'YYYY-MM'DD
// zadziała prawidłowo

db.people.find({
  birth_date: {$gte: '2001-01-01'}
},{_id:0,first_name:1, last_name:1, 'location.city':1});
