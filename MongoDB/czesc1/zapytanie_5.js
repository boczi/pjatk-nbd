// 5. Lista wszystkich osób znajdujących się w bazie o wadze z przedziału <68, 71.5);

// Ponieważ w bazie danych pole weight jest przechowywane jako napis,
// nie ma możliwości prostego porównania wartości.
// Ponieważ w baze danych waga ludzi jest dwucyfrowa, zaryzykuję
// nie działające zawsze rozwiązanie z porównywaniem leksykograficznym.

// UWAGA !!!! tonie zadziała, jeśli w bazie będzie waga =120 !!!

DBQuery.shellBatchSize = 100000;

db.people.find({
  weight: {$gte: '68', $lt: '71.5'}
});
