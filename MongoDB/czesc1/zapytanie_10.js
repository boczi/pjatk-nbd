// 10. Dodaj do wszystkich osób o imieniu Antonio własność
// „hobby” o wartości „pingpong”;

db.people.findOne({first_name:'Antonio'},{first_name:1, hobby:1});

db.people.update(
  {first_name:'Antonio'},
  {$set:{ hobby: 'pingpong'}},
  {multi:true}
);

db.people.findOne({first_name:'Antonio'},{first_name:1, hobby:1});
